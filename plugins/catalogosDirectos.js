import axios from 'axios'
 
let catalogo = process.env.catalogo + "/v2";
class CatalogosDirectos {
  marcas() {
    return axios({
      method: "get",
      url: catalogo + '/qualitas-car/brand'
    })
  }
  modelos(marca) {
    return axios({
      method: "get",
      url: catalogo + `/qualitas-car/year?brand=${marca}`
    })
  }
  submarcas(marca, modelo) {
    return axios({
      method: "get",
      url: catalogo + `/qualitas-car/model?brand=${marca}&year=${modelo}`
    }
    );
  }
  descripciones(marca, modelo, submarca) {
    return axios({
      method: "get",
      url: catalogo + `/qualitas-car/variant?brand=${marca}&year=${modelo}&model=${submarca}`
    }
    );
  }
}
 
export default CatalogosDirectos;