import axios from 'axios'

const monitoreoService = {}

monitoreoService.search = function (idEndPoint,
                                    httpStatus,
                                    message,
                                    idMedioDifusion,
                                    idEstatusIncidencia,
                                    idComponentesAfectados,
                                    accessToken)
{
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
// PRODUCCION
    url: process.env.urlMonitoreo + '/monitoreo',
// PRUEBAS
    // url: process.env.urlMonitoreo + '/pruebas/monitoreo',
    data: {
      idEndPoint,
      httpStatus,
      message,
      idMedioDifusion,
      idEstatusIncidencia,
      idComponentesAfectados
    }
  })
}
export default monitoreoService


