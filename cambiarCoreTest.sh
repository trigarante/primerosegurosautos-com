#! /bin/bash
grep -rl '"https://dev.core-brandingservice.com"' nuxt.config.js | xargs sed -i 's/\"https:\/\/dev.core-brandingservice.com\"/\"https:\/\/core-brandingservice.com\"/g'
grep -rl '"https://dev.ws-qualitas.com"' nuxt.config.js | xargs sed -i 's/\"https:\/\/dev.ws-qualitas.com\"/\"https:\/\/ws-qualitas.com\"/g'
