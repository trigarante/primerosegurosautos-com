module.exports = {
  target: "static",
  /*
   ** Headers of the page
   */
  head: {
    title: "mejorseguro",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Template to pages from mejorseguro",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "./favicon.png" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    // vendor: ["jquery", "bootstrap"],
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          //loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
  // include bootstrap css
  css: ["static/css/bootstrap.min.css", "static/css/styles.css"],
  // include bootstrap js on startup
  //plugins: [{ src: '~/plugins/filters.js', ssr: false }],
  plugins: [{ src: "~/plugins/filters.js", ssr: false }],
  modules: ["@nuxtjs/axios"],
  gtm: { id: "GTM-TN23FLN" },
  env: {
    tokenData: "2/SfiAuJmICCW2WurfZsVbTvNsjLtrWdQ3gsHLqLSjE=", //TOKEN DATA
    catalogo: "https://dev.ws-qualitas.com", //CATALOGO
    coreBranding: "https://dev.core-brandingservice.com", //CORE
    urlValidaciones: "https://core-blacklist-service.com/rest", //VALIDACIONES
    urlMonitoreo: "https://core-monitoreo-service.com/v1", //MONITOREO
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    compressor: { threshold: 9 },
  },
};
